import React from 'react';
import PropTypes from 'prop-types';

import {Content, Editable, Heading, Img, RichText, Text} from 'editable';

import './Table.scss';

/**
 * Table
 * @description [Description]
 * @example
  <div id="Table"></div>
  <script>
    ReactDOM.render(React.createElement(Components.Table, {
    	title : 'Example Table'
    }), document.getElementById("Table"));
  </script>
 */
const Table = props => {
	const {children, className, body} = props;
	const baseClass = 'table';

	return (
		<table className={`${baseClass} ${className}`}>
			{body ? <Editable content={body} tag="tbody" /> : <tbody>{children}</tbody>}
		</table>
	);
};

Table.defaultProps = {
	children: null,
	className: '',
	body: ''
};

Table.propTypes = {
	children: PropTypes.node,
	className: PropTypes.string,
	body: PropTypes.string
};

export default Table;
